resource "aws_route53_zone" "syncthing" {
  name = replace(var.domain, "/^.*?([^\\.]+\\.[^\\.]+)$/", "$${1}")
  tags = var.aws_tags
}

resource "aws_route53_record" "syncthing-A" {
  zone_id = aws_route53_zone.syncthing.zone_id
  name = var.domain
  type = "A"
  ttl = "60"
  records = [aws_eip.syncthing.public_ip]
}

resource "aws_route53_record" "syncthing-AAAA" {
  zone_id = aws_route53_zone.syncthing.zone_id
  name = var.domain
  type = "AAAA"
  ttl = "60"
  records = aws_instance.syncthing.ipv6_addresses
}
