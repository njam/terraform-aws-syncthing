output "addresses" {
  value = {
    ipv4 = aws_eip.syncthing.public_ip
    ipv6 = aws_instance.syncthing.ipv6_addresses[0]
    domain = var.domain
  }
}
output "login" {
  value = {
    username = var.syncthing_username
    password = local.syncthing_password
  }
}
output "dns-records" {
  value = {
    "${aws_route53_record.syncthing-A.name} ${aws_route53_record.syncthing-A.type}" = aws_route53_record.syncthing-A.records
    "${aws_route53_record.syncthing-AAAA.name} ${aws_route53_record.syncthing-AAAA.type}" = aws_route53_record.syncthing-AAAA.records
  }
}
output "name-servers" {
  value = aws_route53_zone.syncthing.name_servers
}
