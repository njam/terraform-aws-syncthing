variable "aws_access_key" {
  description = "AWS access key"
}
variable "aws_secret_key" {
  description = "AWS secret key"
}
variable "aws_region" {
  description = "AWS region"
  default = "eu-central-1"
}
variable "aws_availability_zone" {
  description = "AWS availability zone"
  default = "eu-central-1b"
}
variable "aws_tags" {
  type = map(string)
  default = {
    application = "syncthing"
  }
}

variable "ec2_instance" {
  description = "EC2 instance type"
  default = "t3.small"
}
variable "ec2_ami" {
  description = "Use a specific AMI ID. Default is to use the latest Debian Stretch."
  default = ""
}
variable "ebs_type" {
  description = "EBS volume type"
  default = "gp2"
}
variable "ebs_size" {
  description = "EBS volume size (GB)"
  default = 100
}

variable "domain" {
  description = "Domain name to connect to the instance"
  default = "syncthing.example.com"
}
variable "syncthing_username" {
  description = "Username for Syncthing web-GUI"
  default = "user"
}
variable "syncthing_password" {
  description = "Password for Syncthing web-GUI"
  # If set to `null` a random password will be generated
  default = null
}
