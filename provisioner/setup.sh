#!/bin/bash
set -eux


# Create and mount EBS filesystem
while ! (lsblk /dev/nvme1n1 >/dev/null); do
  echo "EBS device not available at '/dev/nvme1n1', retrying in 1s..."
  sleep 1
done
if ! (lsblk -no FSTYPE /dev/nvme1n1 | grep -q "ext4"); then
  mkfs -t ext4 /dev/nvme1n1
fi
if ! (grep -q "/var/lib/syncthing" /etc/fstab); then
  UUID=$(lsblk -dno UUID /dev/nvme1n1)
  echo "UUID=\"${UUID}\"	/var/lib/syncthing	ext4	defaults,nofail	0	2" >> /etc/fstab
fi
if ! (mount | grep -q "/var/lib/syncthing"); then
  mkdir -p /var/lib/syncthing
  mount /var/lib/syncthing
fi


# Install syncthing
apt-get update
apt-get install -qy gnupg
wget -qO- https://syncthing.net/release-key.txt | apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" > /etc/apt/sources.list.d/syncthing.list
apt-get update
apt-get install -qy syncthing
echo "fs.inotify.max_user_watches=9999999" > /etc/sysctl.d/90-syncthing.conf
sysctl -p /etc/sysctl.d/90-syncthing.conf


# Create user "syncthing" and start service
if ! (id -u syncthing &>/dev/null); then
  useradd --uid 2222 --home-dir "/var/lib/syncthing" --shell "/bin/bash" syncthing
fi
chown syncthing: /var/lib/syncthing
systemctl enable --now syncthing@syncthing.service


# Enable web-GUI
apt-get install -qy augeas-tools
CONFIG_PATH="/var/lib/syncthing/.config/syncthing/config.xml"
augtool -LA <<-EOF
	set /augeas/load/xml/lens "Xml.lns"
	set /augeas/load/xml/incl "$CONFIG_PATH"
	load
	set /files/$CONFIG_PATH/configuration/gui/address/#text "0.0.0.0:8384"
	set /files/$CONFIG_PATH/configuration/gui/user/#text "${VAR_USERNAME}"
	set /files/$CONFIG_PATH/configuration/gui/password/#text "${VAR_PASSWORD_HASH_ESCAPED}"
	save
EOF
systemctl restart syncthing@syncthing.service


# Create nginx reverse proxy for web-GUI
apt-get install -qy nginx
tee /etc/nginx/sites-available/default <<-EOF
	server {
	  listen 80;
	  listen [::]:80;
	  server_name ${VAR_DOMAIN};
	  location / {
	    proxy_pass http://127.0.0.1:8384/;
	    proxy_read_timeout 600s;
	    proxy_send_timeout 600s;
	  }
	}
EOF
systemctl reload nginx
# Install certbot
apt-get install -qy certbot python-certbot-nginx


# Configure APT unattended upgrades
apt-get install -qy unattended-upgrades apt-listchanges
tee /etc/apt/apt.conf.d/50unattended-upgrades <<-EOF
	Unattended-Upgrade::Origins-Pattern {
	  "o=Debian";
	  "o=Syncthing";
	};
	Unattended-Upgrade::Mail "root";
EOF
