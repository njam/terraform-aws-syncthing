data "aws_ami" "debian" {
  most_recent = true
  filter {
    name = "name"
    values = ["debian-10-amd64-*"]
  }
  owners = ["136693071363"] # see https://wiki.debian.org/Cloud/AmazonEC2Image
}

resource "random_password" "syncthing" {
  length = 15
  special = false
}

locals {
  syncthing_password = var.syncthing_password != null ? var.syncthing_password : random_password.syncthing.result
}

resource "aws_instance" "syncthing" {
  availability_zone = var.aws_availability_zone
  ami = var.ec2_ami != "" ? var.ec2_ami : data.aws_ami.debian.id
  instance_type = var.ec2_instance
  subnet_id = aws_subnet.syncthing.id
  ipv6_address_count = 1
  key_name = aws_key_pair.syncthing.key_name
  vpc_security_group_ids = [aws_security_group.syncthing.id]
  user_data = templatefile("provisioner/setup.sh", {
    VAR_DOMAIN = var.domain,
    VAR_USERNAME = var.syncthing_username,
    VAR_PASSWORD_HASH_ESCAPED = replace(bcrypt(local.syncthing_password), "$", "\\$"),
  })
  lifecycle {
    ignore_changes = [
      user_data,
      ami,
    ]
  }
  tags = var.aws_tags
}

resource "aws_ebs_volume" "syncthing" {
  availability_zone = var.aws_availability_zone
  size = var.ebs_size
  type = var.ebs_type
  tags = var.aws_tags
  lifecycle {
    # Safety mechanism to prevent destroying the EBS volume. Set to "false" to override.
    prevent_destroy = true
  }
}

resource "aws_volume_attachment" "syncthing" {
  device_name = "/dev/sdf" # See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/device_naming.html
  volume_id = aws_ebs_volume.syncthing.id
  instance_id = aws_instance.syncthing.id
}

resource "aws_eip" "syncthing" {
  vpc = true
  instance = aws_instance.syncthing.id
}
