resource "aws_vpc" "syncthing" {
  cidr_block = "10.0.0.0/16"
  assign_generated_ipv6_cidr_block = true
  tags = var.aws_tags
}

resource "aws_subnet" "syncthing" {
  availability_zone = var.aws_availability_zone
  vpc_id = aws_vpc.syncthing.id
  cidr_block = cidrsubnet(aws_vpc.syncthing.cidr_block, 8, 0)
  ipv6_cidr_block = cidrsubnet(aws_vpc.syncthing.ipv6_cidr_block, 8, 0)
  assign_ipv6_address_on_creation = true
  tags = var.aws_tags
}

resource "aws_internet_gateway" "syncthing" {
  vpc_id = aws_vpc.syncthing.id
  tags = var.aws_tags
}

resource "aws_route_table" "syncthing" {
  vpc_id = aws_vpc.syncthing.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.syncthing.id
  }
  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.syncthing.id
  }
  tags = var.aws_tags
}

resource "aws_route_table_association" "syncthing" {
  subnet_id = aws_subnet.syncthing.id
  route_table_id = aws_route_table.syncthing.id
}

resource "aws_security_group" "syncthing" {
  name = "syncthing"
  vpc_id = aws_vpc.syncthing.id
  ingress {
    description = "Allow SSH"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description = "Allow ICMP"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow ICMP"
    from_port = -1
    to_port = -1
    protocol = "icmpv6"
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description = "Allow Syncthing Web-GUI"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description = "Allow Syncthing Web-GUI"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description = "Allow Syncthing protocol"
    from_port = 22000
    to_port = 22000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    description = "Allow all outbound"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = var.aws_tags
}
