provider "aws" {
  version = "~> 2.48.0"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.aws_region
}

provider "random" {
  version = "~> 2.2"
}

resource "aws_key_pair" "syncthing" {
  public_key = file("ssh/syncthing.pub")
  tags = var.aws_tags
}
