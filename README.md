terraform-aws-syncthing
=======================

Deploy [Syncthing](https://syncthing.net/) to AWS EC2 using [Terraform](https://www.terraform.io/).

The following resouces will get created:
- EC2 instance running Debian with Syncthing installed
- VPC, Subnet, Firewall, etc. for networking
- Route53 zone and records for DNS resolution

Provisioning
------------

Clone this repo and initialize Terraform:
```
terraform init
```

Create a pair of SSH keys which will be used to connect to your instance:
```
ssh-keygen -t rsa -b 4096 -P '' -f ssh/syncthing
```

Create a file called `terraform.tfvars` and put at least your *AMI access key* credentials and the *domain* to use in it. Consult [`variables.tf`](variables.tf) for other available configuration options.
```
aws_access_key = "my-access-key"
aws_secret_key = "my-secret-key"
domain = "my-syncthing.com"
```

Let Terraform prepare and present a plan for how to create the infrastructure.
Once you confirm the plan it will it be created on AWS.
```
terraform apply -auto-approve=false
```

Once Terraform successfully created the infrastructure you end up with a [`terraform.tfstate` file](https://www.terraform.io/docs/state/) in the current directory.
Make sure to keep this state file around, as you need it if you want to make changes to your infrastructure later, or want to destroy it.

Also store the created SSH keys from the `ssh/` folder, so you can connect to the instance if needed.

The *apply* command prints *output variables* near the end, which show you where you can access the instance:
```
...

addresses = {
  domain = my-syncthing.com
  ipv4 = 1.2.3.4
  ipv6 = 2a05:d014:9c2:3d00:91c9:4a38:739a:dc9c
}
login = {
  "username" = "user"
  "password" = "q20cLJeeF347cNi"
}
...
```

In the example above you could access the instance as follows:
- **SSH**: `ssh -i ssh/syncthing admin@1.2.3.4`
- **Syncthing address**: `tcp://1.2.3.4:22000` (or with DNS set up at `tcp://my-syncthing.com:22000`)
- **Web GUI**: `http://1.2.3.4` (or with DNS and HTTPS set up at `https://my-syncthing.com`)

Change the password immediately via *Actions* > *Settings*.


### DNS
To access the instance at the desired *domain*, DNS needs to be configured manually.

You can either use the DNS zone that was created on *AWS Route53*, by setting the corresponding name servers at the registrar where the domain was registered:
```
$ terraform output name-servers
ns-1436.awsdns-51.org,
ns-166.awsdns-20.com,
ns-1888.awsdns-44.co.uk,
ns-871.awsdns-44.net
```

Or, if you already have a DNS zone set up for the domain, just add the required records:
```
$ terraform output dns-records
syncthing.within.ch A = [1.2.3.4]
syncthing.within.ch AAAA = [2a05:d014:9c2:3d00:91c9:4a38:739a:dc9c]
```

Once those changes have propagated and caches have invalidated the domain should resolve to the instance's IP:
```
$ dig +short my-syncthing.com A
1.2.3.4
```

#### HTTPS
We can install a certificate from *Let's Encrypt* using [certbot](https://certbot.eff.org/). Make sure that DNS resolution works first.

Log into the instance with SSH and run *certbot* to request a certificate and update the nginx config:
```
sudo certbot run --nginx --non-interactive --redirect --agree-tos --email "your@email.com" --domain "my-syncthing.com"
```

You can now access the web GUI at `https://my-syncthing.com`.

Maintenance
-----------

#### Increase EBS Volume Size
EBS volume size can be changed [on the fly](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modify-volume.html).
First configure the desired new size in the `terraform.tfvars` file:
```
ebs_size = 200
```
Then apply the change with terraform:
```
terraform apply -auto-approve=false
```

Log into the machine with SSH and grow the file system:
```
sudo resize2fs /dev/nvme1n1
```

Note that for shrinking one needs to create a separate volume, and copy the data over.

#### Change EC2 Instance Type
The [EC2 instance type](https://aws.amazon.com/ec2/instance-types/) can be changed easily, since all data resides on EBS.
Override the instance type in `terraform.tfvars` like this:
```
ec2_instance = "t3.medium"
```
Then apply the change with terraform:
```
terraform apply -auto-approve=false
```
After a reboot the new instance will be available.

#### Decommission the infrastructure
If you don't need the installation anymore you can destroy it with terraform:
```
terraform destroy
```
You *will* get an error that `aws_ebs_volume.syncthing` has `lifecycle.prevent_destroy` set to *true*.
This is a safety mechanism to help prevent accidental deletion of the EBS volume with all the data on it.
To turn it off change it in `main.tf` to *false*:
```
prevent_destroy = true
```
Then run the command again.
